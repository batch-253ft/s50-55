// import { useState, useEffect } from 'react';
// import PropTypes from 'prop-types';

import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({ courseProp }) {

	// Checks to see if the data was successfully passed
	// console.log(props);
	// Every component receives information in a form of an object
	// console.log(typeof props);

	// Deconstruct the course properties into their own variables (another way of accessing the courseProp property)
	// const { name, description, price } = courseProp; 

	// Use the state hook for this component to be able to store its state
	// States are used to keep track of information related to individual components
	// Syntax
		// const [getter, setter] = useState(initialGetterValue);
	/*const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(30);
	const [status, setStatus] = useState(false);

	// Using the state hook returns an array with the first element being a value and the second element as a function that's used to change the value of the first element
	// console.log(useState(0));

	function enroll() {

		if (seats == 0) {
			alert("No more seats.");
			setStatus(true);
		} else {
			setCount(count + 1);
			setSeats(seats - 1);
		}
		console.log(`${count} Enrollees`);
		console.log(`${seats} Seats`);
	};*/

	// Define a "useEffect" hook to have the "CourseCard" component do perform a certain task after every DOM update
    // This is run automatically both after initial render and for every DOM update
    // Checking for the availability for enrollment of a course is better suited here
    // [seats] is an OPTIONAL parameter
    // React will re-run this effect ONLY if any of the values contained in this array has changed from the last render / update
	/*useEffect(() => {
		if (seats === 0) {
			setSeats(false);
		}
	}, [seats]);*/


	return (
				<Card>
			     <Card.Body>
			       <Card.Title>
			       	<h5>{ courseProp.name }</h5>
			       </Card.Title>
			       <Card.Subtitle>Description</Card.Subtitle>
			       <Card.Text>{ courseProp.description }</Card.Text>
			       <Card.Subtitle>Price</Card.Subtitle>
			       <Card.Text>{ courseProp.price }</Card.Text>
			       <Link className="btn btn-primary" to={`/courses/${courseProp._id}`}>Details</Link>
			     </Card.Body>
			   </Card>
	)
}

// Check if the CourseCard component is getting the correct prop types
// Proptypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is passed from one component to the next
/*CourseCard.PropTypes = {

	// The "shape" method is used to check if a prop object conforms to a specific "shape"
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}*/