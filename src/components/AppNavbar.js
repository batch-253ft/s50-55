// React Bootstrap Components
/*
	import moduleName from "filePath"
*/

// import { useState } from 'react';
import { useContext } from 'react';

// import Container from "react-bootstrap/Container";
import { Container, Navbar, Nav } from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";

import UserContext from '../UserContext';

export default function AppNavBar() {

	// State to store the user information stored in the login page.
	// const [user, setUser] = useState(localStorage.getItem('email'));
	// console.log(user);

	const { user } = useContext(UserContext);

	return (
		<Navbar bg="light" expand="lg">
			<Container fluid>
				<Navbar.Brand as={ Link } to="/">Zuitt Booking</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ms-auto">
					{/*The "as" prop allows components to be treated as if they are a different component gaining access to it's properties and functionalities.*/}
						<Nav.Link as={ NavLink } to="/" exact="true">Home</Nav.Link>
						{/*- The "to" prop is used in place of the "href" prop for providing the URL for the page.*/}
						<Nav.Link as={ NavLink } to="/courses" exact="true">Courses</Nav.Link>

						{ (user.id !== null) ?
							<Nav.Link as={ NavLink } to="/logout" exact="true">Logout</Nav.Link>
							:
							<>
								<Nav.Link as={ NavLink } to="/login" exact="true">Login</Nav.Link>
								<Nav.Link as={ NavLink } to="/register" exact="true">Register</Nav.Link>
							</>
						}

						{/*- The "exact" prop is used to highlight the active NavLink component that matches the URL.*/}
						
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	)
}