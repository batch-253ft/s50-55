import { Row, Col, Nav } from 'react-bootstrap';
import { Link } from "react-router-dom";

export default function Banner() {

	return (
		<Row>
			<Col className="p-5">
				<h1>Page Not Found</h1>
				<p>
					Go back to the <Link to="/" style={{ textDecoration : "none" }}>homepage</Link>.
				</p>
			</Col>
		</Row>
	);
}