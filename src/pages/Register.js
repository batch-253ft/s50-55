import { useState, useEffect, useContext } from 'react';

import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

import UserContext from '../UserContext';

export default function Register() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	// State hooks to store the values of the input fields
	// getters are variables that store data (from setters)
	// setters are function that sets the data (for the getters)
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);

	// Function to simulate user registration
	function registerUser(e) {
		// Prevents page redirection via form submission
		e.preventDefault();

		// Clear input fields
		setFirstName('');
		setLastName('');
		setMobileNo('');
		setEmail('');
		setPassword1('');
		setPassword2('');
	}

	useEffect(() => {
		// Validation to enable submit button when all fields are populated and both passwords match
		if ((firstName !== '' && lastName !== '' && mobileNo !== "" && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [firstName, lastName, mobileNo, email, password1, password2]);

	function registerUser(e) {

		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmail', 
		{
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {

			if (data === false) {

				fetch('http://localhost:4000/users/register', 
				{
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNo,
						email: email,
						password: password2
					})
				})
				.then(res => res.json())
				.then(data => {

					console.log(data);

					Swal.fire({
						title: "Registration Successful",
						icon: "success",
						text: "Welcome to Zuitt!"
					});
				});

				navigate('/login');

			} else {

				Swal.fire({
					title: "Duplicate Email Found",
					icon: "error",
					text: "Please provide a different email."
				});
			}
		})
	}

	return (
		(user.id !== null) ?
			<Navigate to="/courses" />
		:
			// 2-way Binding (Binding the user input states into their corresponding input fields via the onChange JSX Event Handler) 
			<Form onSubmit={ registerUser }>
		      
		      <Form.Group className="mb-3" controlId="userFirstName">
		        <Form.Label>First Name</Form.Label>
		        <Form.Control 
		        	type="text" 
		        	placeholder="Enter First Name" 
		        	value={ firstName }
		        	onChange={ e => setFirstName(e.target.value) }
		        	required/>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="userLastName">
		        <Form.Label>Last Name</Form.Label>
		        <Form.Control 
		        	type="text" 
		        	placeholder="Enter Last Name" 
		        	value={ lastName }
		        	onChange={ e => setLastName(e.target.value) }
		        	required/>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="userMobileNo">
		        <Form.Label>Mobile Number</Form.Label>
		        <Form.Control 
		        	type="text" 
		        	placeholder="Enter Mobile Number" 
		        	value={ mobileNo }
		        	minLength={11}
		        	maxLength={11}
		        	onChange={ e => setMobileNo(e.target.value) }
		        	required/>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="userEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control 
		        	type="email" 
		        	placeholder="Enter email" 
		        	value={ email }
		        	onChange={ e => setEmail(e.target.value) }
		        	required/>
		        <Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		        </Form.Text>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="password1">
		        <Form.Label>Password</Form.Label>
		        <Form.Control 
		        	type="password" 
		        	placeholder="Password"
		        	value={ password1 }
		        	onChange={ e => setPassword1(e.target.value) } 
		        	required/>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="password2">
		        <Form.Label>Verify Password</Form.Label>
		        <Form.Control 
		        	type="password" 
		        	placeholder="Verify Password"
		        	value={ password2 }
		        	onChange={ e => setPassword2(e.target.value) }
		        	required/>
		      </Form.Group>

		      { isActive ? 
		      		<Button variant="primary" type="submit" id="submitBtn">
		      		  Submit
		      		</Button>
		      		:
		      		<Button variant="danger" type="submit" id="submitBtn" disabled>
		      		  Submit
		      		</Button>
		  		}
		    </Form>
	)
}