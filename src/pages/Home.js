import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
// import CourseCard from '../components/CourseCard'; will be imported to Courses page

export default function Home() {
	return (
		<>
			<Banner />
			<Highlights />
			{/*<CourseCard />*/}
		</>
	)
}